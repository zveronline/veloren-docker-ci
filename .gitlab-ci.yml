stages:
    - base-start
    - base-platform
    - cache 

variables:
    BRANCH:
      value: "master"
      description: "This is the veloren branch used to build the cache. For example, this can be set to a custom branch when performing toolchain upgrades that may need changes to still compile properly."

# Template for building docker images with kaniko
.template-kaniko:
    image:
      name: gcr.io/kaniko-project/executor:debug
      entrypoint: [""]
    tags: ["veloren/veloren-docker-ci", "trusted", "docker", "gitlab-org-docker"]
    before_script:
        - echo "{\"auths\":{\"$CI_REGISTRY\":{\"username\":\"$CI_REGISTRY_USER\",\"password\":\"$CI_REGISTRY_PASSWORD\"}}}" > /kaniko/.docker/config.json
        # Help kaniko identify that it is running in a container.
        # avoids this issue: https://github.com/GoogleContainerTools/kaniko/issues/1542
        - export container=docker

# STAGE: base-start

.template-base-common:
    extends: .template-kaniko
    stage: base-start
    script:
        - /kaniko/executor --context $CI_PROJECT_DIR --dockerfile $CI_PROJECT_DIR/base/common.Dockerfile --destination "${CI_REGISTRY_IMAGE}/base/common:${CI_COMMIT_SHORT_SHA}"

.template-base-osxcross-x86_64:
    extends: .template-kaniko
    stage: base-start
    script:
        - /kaniko/executor --context $CI_PROJECT_DIR --dockerfile $CI_PROJECT_DIR/base/osxcross-x86_64.Dockerfile --destination "${CI_REGISTRY_IMAGE}/base/osxcross-x86_64:${CI_COMMIT_SHORT_SHA}"

.template-base-shaderc-linux-x86_64:
    extends: .template-kaniko
    stage: base-start
    script:
        - /kaniko/executor --context $CI_PROJECT_DIR --dockerfile $CI_PROJECT_DIR/base/shaderc/linux-x86_64.Dockerfile --destination "${CI_REGISTRY_IMAGE}/base/shaderc-linux-x86_64:${CI_COMMIT_SHORT_SHA}"

.template-base-shaderc-macos-x86_64:
    extends: .template-kaniko
    stage: base-start
    script:
        - /kaniko/executor --context $CI_PROJECT_DIR --dockerfile $CI_PROJECT_DIR/base/shaderc/macos-x86_64.Dockerfile --destination "${CI_REGISTRY_IMAGE}/base/shaderc-macos-x86_64:${CI_COMMIT_SHORT_SHA}"

# STAGE: base-platform

.template-base-linux-x86_64:
  extends: .template-kaniko
  stage: base-platform
  script:
    - /kaniko/executor --context $CI_PROJECT_DIR --dockerfile $CI_PROJECT_DIR/base/linux-x86_64.Dockerfile --build-arg FROM_TAG=$CI_COMMIT_SHORT_SHA --destination "${CI_REGISTRY_IMAGE}/base/linux-x86_64:${CI_COMMIT_SHORT_SHA}"

.template-base-linux-aarch64:
  extends: .template-kaniko
  stage: base-platform
  script:
    - /kaniko/executor --context $CI_PROJECT_DIR --dockerfile $CI_PROJECT_DIR/base/linux-aarch64.Dockerfile --build-arg FROM_TAG=$CI_COMMIT_SHORT_SHA --destination "${CI_REGISTRY_IMAGE}/base/linux-aarch64:${CI_COMMIT_SHORT_SHA}"

.template-base-windows-x86_64:
  extends: .template-kaniko
  stage: base-platform
  script:
    - /kaniko/executor --context $CI_PROJECT_DIR --dockerfile $CI_PROJECT_DIR/base/windows-x86_64.Dockerfile --build-arg FROM_TAG=$CI_COMMIT_SHORT_SHA --destination "${CI_REGISTRY_IMAGE}/base/windows-x86_64:${CI_COMMIT_SHORT_SHA}"

.template-base-macos-x86_64:
  extends: .template-kaniko
  stage: base-platform
  script:
    - /kaniko/executor --context $CI_PROJECT_DIR --dockerfile $CI_PROJECT_DIR/base/macos-x86_64.Dockerfile --build-arg FROM_TAG=$CI_COMMIT_SHORT_SHA --destination "${CI_REGISTRY_IMAGE}/base/macos-x86_64:${CI_COMMIT_SHORT_SHA}"

# STAGE: cache

.template-cache-bench:
  extends: .template-kaniko
  stage: cache
  script:
    - /kaniko/executor --context $CI_PROJECT_DIR --dockerfile $CI_PROJECT_DIR/cache/bench.Dockerfile --build-arg FROM_TAG=$CI_COMMIT_SHORT_SHA --build-arg BRANCH=$BRANCH --destination "${CI_REGISTRY_IMAGE}/cache/bench:${CI_COMMIT_SHORT_SHA}" --destination "${CI_REGISTRY_IMAGE}/cache/bench:latest" --push-retry 5

.template-cache-quality:
  extends: .template-kaniko
  stage: cache
  script:
    - /kaniko/executor --context $CI_PROJECT_DIR --dockerfile $CI_PROJECT_DIR/cache/quality.Dockerfile --build-arg FROM_TAG=$CI_COMMIT_SHORT_SHA --build-arg BRANCH=$BRANCH --destination "${CI_REGISTRY_IMAGE}/cache/quality:${CI_COMMIT_SHORT_SHA}" --destination "${CI_REGISTRY_IMAGE}/cache/quality:latest" --push-retry 5

.template-cache-tarpaulin:
  extends: .template-kaniko
  stage: cache
  script:
    - /kaniko/executor --context $CI_PROJECT_DIR --dockerfile $CI_PROJECT_DIR/cache/tarpaulin.Dockerfile --build-arg FROM_TAG=$CI_COMMIT_SHORT_SHA --build-arg BRANCH=$BRANCH --destination "${CI_REGISTRY_IMAGE}/cache/tarpaulin:${CI_COMMIT_SHORT_SHA}" --destination "${CI_REGISTRY_IMAGE}/cache/tarpaulin:latest" --push-retry 5

.template-cache-release-linux-x86_64:
  extends: .template-kaniko
  stage: cache
  script:
    - /kaniko/executor --context $CI_PROJECT_DIR --dockerfile $CI_PROJECT_DIR/cache/release/linux-x86_64.Dockerfile --build-arg FROM_TAG=$CI_COMMIT_SHORT_SHA --build-arg BRANCH=$BRANCH --destination "${CI_REGISTRY_IMAGE}/cache/release-linux-x86_64:${CI_COMMIT_SHORT_SHA}" --destination "${CI_REGISTRY_IMAGE}/cache/release-linux-x86_64:latest" --push-retry 5

.template-cache-release-linux-aarch64:
  extends: .template-kaniko
  stage: cache
  script:
    - /kaniko/executor --context $CI_PROJECT_DIR --dockerfile $CI_PROJECT_DIR/cache/release/linux-aarch64.Dockerfile --build-arg FROM_TAG=$CI_COMMIT_SHORT_SHA --build-arg BRANCH=$BRANCH --destination "${CI_REGISTRY_IMAGE}/cache/release-linux-aarch64:${CI_COMMIT_SHORT_SHA}" --destination "${CI_REGISTRY_IMAGE}/cache/release-linux-aarch64:latest" --push-retry 5

.template-cache-release-windows-x86_64:
  extends: .template-kaniko
  stage: cache
  script:
    - /kaniko/executor --context $CI_PROJECT_DIR --dockerfile $CI_PROJECT_DIR/cache/release/windows-x86_64.Dockerfile --build-arg FROM_TAG=$CI_COMMIT_SHORT_SHA --build-arg BRANCH=$BRANCH --destination "${CI_REGISTRY_IMAGE}/cache/release-windows-x86_64:${CI_COMMIT_SHORT_SHA}" --destination "${CI_REGISTRY_IMAGE}/cache/release-windows-x86_64:latest" --push-retry 5

.template-cache-release-macos-x86_64:
  extends: .template-kaniko
  stage: cache
  script:
    - /kaniko/executor --context $CI_PROJECT_DIR --dockerfile $CI_PROJECT_DIR/cache/release/macos-x86_64.Dockerfile --build-arg FROM_TAG=$CI_COMMIT_SHORT_SHA --build-arg BRANCH=$BRANCH --destination "${CI_REGISTRY_IMAGE}/cache/release-macos-x86_64:${CI_COMMIT_SHORT_SHA}" --destination "${CI_REGISTRY_IMAGE}/cache/release-macos-x86_64:latest" --push-retry 5

#
# Actual jobs built with templates
#

base-common:
    extends: .template-base-common

# base-osxcross-x86_64:
#     extends: .template-base-osxcross-x86_64

base-shaderc-linux-x86_64:
    extends: .template-base-shaderc-linux-x86_64

# base-shaderc-macos-x86_64:
    # extends: .template-base-shaderc-macos-x86_64

base-linux-x86_64:
    extends: .template-base-linux-x86_64
    needs: ["base-common", "base-shaderc-linux-x86_64"]

base-windows-x86_64:
    extends: .template-base-windows-x86_64
    needs: ["base-common"]

base-linux-aarch64:
    extends: .template-base-linux-aarch64
    needs: ["base-common"]

# base-macos-x86_64:
#    extends: .template-base-macos-x86_64
#    needs: ["base-common", "base-osxcross-x86_64"]
#    #needs: ["base-common", "base-osxcross-x86_64", "base-shaderc-macos-x86_64"]

cache-bench:
    extends: .template-cache-bench
    needs: ["base-linux-x86_64"]

cache-quality:
    extends: .template-cache-quality
    needs: ["base-linux-x86_64"]

cache-tarpaulin:
    extends: .template-cache-tarpaulin
    needs: ["base-linux-x86_64"]
 
cache-release-linux-x86_64:
    extends: .template-cache-release-linux-x86_64
    needs: ["base-linux-x86_64"]
 
cache-release-windows-x86_64:
    extends: .template-cache-release-windows-x86_64
    needs: ["base-windows-x86_64"]

cache-release-linux-aarch64:
    extends: .template-cache-release-linux-aarch64
    needs: ["base-linux-aarch64"]

# cache-release-macos-x86_64:
#    extends: .template-cache-release-macos-x86_64
#    needs: ["base-macos-x86_64"]

# Workflow
workflow:
  rules:
    - if: $CI_PIPELINE_SOURCE == "web"
    - if: $CI_PIPELINE_SOURCE == "schedules"

#nightly:
#   extends: .template-cache
#   only:
#     - schedules
