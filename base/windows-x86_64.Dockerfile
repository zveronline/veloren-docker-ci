FROM registry.gitlab.com/veloren/veloren-docker-ci/base/common:$FROM_TAG

# Install necessary packages for windows cross compile
RUN apt-get update \
    && DEBIAN_FRONTEND=noninteractive \
    && time apt-get install -y --no-install-recommends --assume-yes \
        # Needed to cross compile to windows
        gcc-mingw-w64-x86-64 \
        gcc \
        libc-dev \
        python3 \
        g++-mingw-w64-x86-64 \
        # Required to build NSIS Windows installers (used by Airshipper)
        nsis \
    # Cleanup extra cached files
    && rm -rf /var/lib/apt/lists/*;

# Install the NSIS EnVar plugin
COPY base/nsis_plugins/x86-unicode/EnVar.dll /usr/share/nsis/Plugins/x86-unicode/

# Install windows gnu toolchain
RUN echo \
        "[target.x86_64-pc-windows-gnu]\nlinker = \"/usr/bin/x86_64-w64-mingw32-gcc\"\n" \
        >> /root/.cargo/config \
    && . /root/.cargo/env \
    && time rustup target add x86_64-pc-windows-gnu
