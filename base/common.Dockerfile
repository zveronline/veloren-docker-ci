FROM ubuntu:20.04 as base

ARG RUST_TOOLCHAIN=nightly-2023-04-20
ARG PROJECTNAME=veloren

ENV RUST_TOOLCHAIN=$RUST_TOOLCHAIN
ENV PROJECTNAME=$PROJECTNAME
ENV DEBIAN_FRONTEND=noninteractive

# Disable incremental building for the CI
# Recommended here: https://matklad.github.io/2021/09/04/fast-rust-builds.html
# E.g. 1.9 GB of the 5.9 GB target folder for the cache/quality image was the incremental folder
ENV CARGO_INCREMENTAL=0

RUN apt-get update \
    && DEBIAN_FRONTEND=noninteractive \
    && apt-get install -y --no-install-recommends --assume-yes \
        time \
        curl \
        ca-certificates \
        # Used in veloren-common crate build script and cache Dockerfiles
        git \
        git-lfs \
        # For shaderc
        cmake \
        make \
    # Cleanup extra cached files
    && rm -rf /var/lib/apt/lists/*;

RUN time curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh -s -- -y --default-toolchain ${RUST_TOOLCHAIN} --profile=minimal

# Download cargo registry index
# TODO: if a better way is known replace this
# TODO: consider omitting this here and doing it during cache building
RUN . /root/.cargo/env \
    && cargo new fetch-index \
    && cd fetch-index \
    && echo 'serde = "*"' >> Cargo.toml \
    && cargo fetch \
    && cd .. \
    && rm -r fetch-index;

