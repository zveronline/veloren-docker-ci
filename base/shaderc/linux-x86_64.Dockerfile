FROM ubuntu:18.04 as fetch

RUN apt-get update \
    && DEBIAN_FRONTEND=noninteractive \
    && apt-get install -y --no-install-recommends --assume-yes \
        time \
        # ~12 mb
        curl \
        # ~2.8 mb
        ca-certificates \
        # ~ 6 mb
        gnupg2;

RUN time curl --proto '=https' --tlsv1.2 -sSf https://packages.lunarg.com/lunarg-signing-key-pub.asc | apt-key add -
RUN time curl --proto '=https' --tlsv1.2 -sSf https://packages.lunarg.com/vulkan/1.3.216/lunarg-vulkan-1.3.216-bionic.list > /etc/apt/sources.list.d/lunarg-vulkan-1.3.216-bionic.list

RUN apt-get update \
    && DEBIAN_FRONTEND=noninteractive \
    && apt-get install -y --no-install-recommends --assume-yes \
        # ~ 31.3 mb
        shaderc;

RUN mkdir /shaderc /shaderc/shared /shaderc/combined \
    && cp /usr/lib/x86_64-linux-gnu/libshaderc_shared.so.1 /shaderc/shared/libshaderc_shared.so \
    && cp /usr/lib/x86_64-linux-gnu/libshaderc_combined.a /shaderc/combined/libshaderc_combined.a

FROM scratch
COPY --from=fetch /shaderc /shaderc
